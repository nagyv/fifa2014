import sys, os
from pip.req import parse_requirements
from setuptools import setup, find_packages
from setuptools.command.test import test as TestCommand

# parse_requirements() returns generator of pip.req.InstallRequirement objects
prod_install_reqs = parse_requirements('requirements.pip')
# dev_install_req = parse_requirements('requirements/dev.txt')

# reqs is a list of requirement
# e.g. ['django==1.5.1', 'mezzanine==1.4.6']
prod_reqs = [str(ir.req) for ir in prod_install_reqs]
# dev_reqs = [str(ir.req) for ir in dev_install_req]

class PyTest(TestCommand):
    def finalize_options(self):
        TestCommand.finalize_options(self)
        self.test_args = []
        self.test_suite = True

    def run_tests(self):
        #import here, cause outside the eggs aren't loaded
        import pytest
        errno = pytest.main(self.test_args)
        sys.exit(errno)

setup(
    name="Skyfall fifa",
    version="0.0",
    long_description="Fifa",
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    # install_requires=prod_reqs,  # these don't work because of -e lines in the requirements
    # tests_require=dev_reqs,
    tests_require=['pytest', 'pytest-cov', 'pytest-xdist'],
    cmdclass = {
        'test': PyTest,
    }
)
