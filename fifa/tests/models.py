from datetime import date, timedelta
from django.test import TestCase
from ..models import Liga, Csapat, Ember, Bajnoksag, Merkozes, Selejtezo

class LigaTest(TestCase):

    def setUp(self):
        super(LigaTest, self).setUp()
        ember = Ember.objects.create(name="Ember")
        map(lambda idx: ember.csapat_set.create(name="Csapat %d" % idx), range(1,11))
        bajnoksag = Bajnoksag.objects.create(idopont=date.today())
        liga = Liga(bajnoksag=bajnoksag, tipus="A")
        map(lambda idx: setattr(liga, 'cs%d' % idx, ember.csapat_set.all()[idx-1]), range(1, 11))
        liga.save()
        self.liga = liga
    #
    # def test_csapatok(self):
    #     assert type(self.liga.csapatok()) == 'generator'

    def test_merkozesek_generalasa(self):
        self.liga.merkozesek_generalasa()
        assert Merkozes.objects.all().count() == 45


class BajnoksagTest(TestCase):

    def test_ligak_generalasa(self):
        ember = Ember.objects.create(name="Ember")
        map(lambda idx: ember.csapat_set.create(name="Csapat %d" % idx), range(1,17))
        bajnoksag = Bajnoksag.objects.create(idopont=date.today()-timedelta(days=1))
        ligaA = Liga(bajnoksag=bajnoksag, tipus="A")
        map(lambda idx: setattr(ligaA, 'cs%d' % idx, ember.csapat_set.all()[idx-1]), range(1, 11))
        map(lambda idx: setattr(ligaA, 'pos%d' % idx, ember.csapat_set.all()[idx-1]), range(1, 11))
        ligaA.save()

        ligaB = Liga(bajnoksag=bajnoksag, tipus="B")
        map(lambda idx: setattr(ligaB, 'cs%d' % idx, ember.csapat_set.all()[idx-1]), range(1, 11))
        map(lambda idx: setattr(ligaB, 'pos%d' % idx, ember.csapat_set.all()[idx-1]), range(1, 11))
        ligaB.save()

        ligaC = Liga(bajnoksag=bajnoksag, tipus="C")
        map(lambda idx: setattr(ligaC, 'cs%d' % idx, ember.csapat_set.all()[idx-1]), range(1, 11))
        map(lambda idx: setattr(ligaC, 'pos%d' % idx, ember.csapat_set.all()[idx-1]), range(1, 11))
        ligaC.save()

        bajnoksag.osztalyozo_A7_B4 = Merkozes.objects.create(home=ligaA.cs7, away=ligaB.cs4, bajnoksag=bajnoksag, home_score=0, away_score=1)
        bajnoksag.osztalyozo_A8_B3 = Merkozes.objects.create(home=ligaA.cs8, away=ligaB.cs3, bajnoksag=bajnoksag, home_score=0, away_score=1)
        bajnoksag.osztalyozo_B7_C4 = Merkozes.objects.create(home=ligaB.cs7, away=ligaC.cs4, bajnoksag=bajnoksag, home_score=0, away_score=1)
        bajnoksag.osztalyozo_B8_C3= Merkozes.objects.create(home=ligaB.cs8, away=ligaC.cs3, bajnoksag=bajnoksag, home_score=0, away_score=1)
        bajnoksag.save()

        selejt = Selejtezo(bajnoksag=bajnoksag)
        map(lambda idx: setattr(selejt, 'cs%d' % idx, ember.csapat_set.all()[idx-1]), range(1, 17))
        selejt.elso = selejt.cs1
        selejt.masodik = selejt.cs2
        selejt.harmadik = selejt.cs3
        selejt.negyedik = selejt.cs4
        selejt.save()

        bajnoksag2 = Bajnoksag.objects.create(idopont=date.today())
        bajnoksag2.ligak_generalasa()

        assert bajnoksag2.liga_set.count() == 3
