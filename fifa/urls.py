from django.conf.urls import patterns, include, url
from django.conf import settings
from django.views.generic import RedirectView

import autocomplete_light
# import every app/autocomplete_light_registry.py
autocomplete_light.autodiscover()

from django.contrib import admin
admin.autodiscover()

from . import views

urlpatterns = patterns('',
    # Examples:
    url(r'^$', RedirectView.as_view(url='csapatok/'), name='home'),
    url(r'^emberek/$', views.EmberList.as_view(), name='emberek'),
    url(r'^csapatok/$', views.CsapatList.as_view(), name='csapatok'),
    url(r'^bajnoksagok/$', views.BajnoksagList.as_view(), name='bajnoksagok'),
    # url(r'^bajnoksagok/(?P<pk>\d+)/$', views.BajnoksagView.as_view(), name='bajnoksag'),
    url(r'^bajnoksagok/(?P<pk>\d+)$', views.LigaView.as_view(), name='liga'),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^autocomplete/', include('autocomplete_light.urls')),
)

if settings.DEBUG:
    urlpatterns = patterns('',
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
        {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
    url(r'', include('django.contrib.staticfiles.urls')),
) + urlpatterns