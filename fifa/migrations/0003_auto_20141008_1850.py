# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('fifa', '0002_auto_20140914_2002'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='csapat',
            options={'ordering': ['-pontszam', 'ea_pontok']},
        ),
        migrations.AlterModelOptions(
            name='ember',
            options={'ordering': ['-pontszam', 'name']},
        ),
        migrations.AlterModelOptions(
            name='liga',
            options={'ordering': ['bajnoksag', 'tipus']},
        ),
        migrations.AddField(
            model_name='merkozes',
            name='gol',
            field=models.ManyToManyField(to='fifa.Golszerzo', through='fifa.Golok'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='merkozes',
            name='away',
            field=models.ForeignKey(related_name=b'merkozes_away', to='fifa.Csapat'),
        ),
        migrations.AlterField(
            model_name='merkozes',
            name='home',
            field=models.ForeignKey(related_name=b'merkozes_home', to='fifa.Csapat'),
        ),
        migrations.AlterField(
            model_name='merkozes',
            name='liga',
            field=models.ForeignKey(blank=True, to='fifa.Liga', null=True),
        ),
    ]
