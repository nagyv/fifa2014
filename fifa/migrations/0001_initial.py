# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone
import model_utils.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Bajnoksag',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, verbose_name='created', editable=False)),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, verbose_name='modified', editable=False)),
                ('idopont', models.DateField()),
            ],
            options={
                'ordering': ['idopont'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Csapat',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('fifa_rangsor', models.SmallIntegerField(null=True, blank=True)),
                ('ea_pontok', models.SmallIntegerField(null=True, blank=True)),
                ('pontszam', models.IntegerField(default=0)),
            ],
            options={
                'ordering': ['pontszam'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Ember',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, verbose_name='created', editable=False)),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, verbose_name='modified', editable=False)),
                ('name', models.CharField(max_length=255)),
                ('pontszam', models.IntegerField(default=0)),
            ],
            options={
                'ordering': ['pontszam'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Golok',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, verbose_name='created', editable=False)),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, verbose_name='modified', editable=False)),
                ('golok', models.SmallIntegerField(default=0)),
                ('ongol', models.BooleanField(default=False)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Golszerzo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, verbose_name='created', editable=False)),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, verbose_name='modified', editable=False)),
                ('name', models.CharField(max_length=255)),
                ('osszgol', models.IntegerField(default=0)),
                ('csapat', models.ForeignKey(to='fifa.Csapat')),
            ],
            options={
                'ordering': ['osszgol'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Liga',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, verbose_name='created', editable=False)),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, verbose_name='modified', editable=False)),
                ('tipus', models.CharField(max_length=1, choices=[(b'A', b'A'), (b'B', b'B'), (b'C', b'C')])),
                ('bajnoksag', models.ForeignKey(to='fifa.Bajnoksag')),
                ('cs1', models.ForeignKey(related_name=b'+', to='fifa.Csapat')),
                ('cs10', models.ForeignKey(related_name=b'+', to='fifa.Csapat')),
                ('cs2', models.ForeignKey(related_name=b'+', to='fifa.Csapat')),
                ('cs3', models.ForeignKey(related_name=b'+', to='fifa.Csapat')),
                ('cs4', models.ForeignKey(related_name=b'+', to='fifa.Csapat')),
                ('cs5', models.ForeignKey(related_name=b'+', to='fifa.Csapat')),
                ('cs6', models.ForeignKey(related_name=b'+', to='fifa.Csapat')),
                ('cs7', models.ForeignKey(related_name=b'+', to='fifa.Csapat')),
                ('cs8', models.ForeignKey(related_name=b'+', to='fifa.Csapat')),
                ('cs9', models.ForeignKey(related_name=b'+', to='fifa.Csapat')),
                ('pos1', models.ForeignKey(related_name=b'+', blank=True, to='fifa.Csapat', null=True)),
                ('pos10', models.ForeignKey(related_name=b'+', blank=True, to='fifa.Csapat', null=True)),
                ('pos2', models.ForeignKey(related_name=b'+', blank=True, to='fifa.Csapat', null=True)),
                ('pos3', models.ForeignKey(related_name=b'+', blank=True, to='fifa.Csapat', null=True)),
                ('pos4', models.ForeignKey(related_name=b'+', blank=True, to='fifa.Csapat', null=True)),
                ('pos5', models.ForeignKey(related_name=b'+', blank=True, to='fifa.Csapat', null=True)),
                ('pos6', models.ForeignKey(related_name=b'+', blank=True, to='fifa.Csapat', null=True)),
                ('pos7', models.ForeignKey(related_name=b'+', blank=True, to='fifa.Csapat', null=True)),
                ('pos8', models.ForeignKey(related_name=b'+', blank=True, to='fifa.Csapat', null=True)),
                ('pos9', models.ForeignKey(related_name=b'+', blank=True, to='fifa.Csapat', null=True)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Merkozes',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, verbose_name='created', editable=False)),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, verbose_name='modified', editable=False)),
                ('home_score', models.SmallIntegerField(null=True, blank=True)),
                ('away_score', models.SmallIntegerField(null=True, blank=True)),
                ('away', models.ForeignKey(related_name=b'+', to='fifa.Csapat')),
                ('bajnoksag', models.ForeignKey(to='fifa.Bajnoksag')),
                ('home', models.ForeignKey(related_name=b'+', to='fifa.Csapat')),
                ('liga', models.ForeignKey(to='fifa.Liga', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Selejtezo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, verbose_name='created', editable=False)),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, verbose_name='modified', editable=False)),
                ('bajnoksag', models.ForeignKey(to='fifa.Bajnoksag')),
                ('cs1', models.ForeignKey(related_name=b'+', to='fifa.Csapat')),
                ('cs10', models.ForeignKey(related_name=b'+', to='fifa.Csapat')),
                ('cs11', models.ForeignKey(related_name=b'+', to='fifa.Csapat')),
                ('cs12', models.ForeignKey(related_name=b'+', to='fifa.Csapat')),
                ('cs13', models.ForeignKey(related_name=b'+', to='fifa.Csapat')),
                ('cs14', models.ForeignKey(related_name=b'+', to='fifa.Csapat')),
                ('cs15', models.ForeignKey(related_name=b'+', to='fifa.Csapat')),
                ('cs16', models.ForeignKey(related_name=b'+', to='fifa.Csapat')),
                ('cs2', models.ForeignKey(related_name=b'+', to='fifa.Csapat')),
                ('cs3', models.ForeignKey(related_name=b'+', to='fifa.Csapat')),
                ('cs4', models.ForeignKey(related_name=b'+', to='fifa.Csapat')),
                ('cs5', models.ForeignKey(related_name=b'+', to='fifa.Csapat')),
                ('cs6', models.ForeignKey(related_name=b'+', to='fifa.Csapat')),
                ('cs7', models.ForeignKey(related_name=b'+', to='fifa.Csapat')),
                ('cs8', models.ForeignKey(related_name=b'+', to='fifa.Csapat')),
                ('cs9', models.ForeignKey(related_name=b'+', to='fifa.Csapat')),
                ('elso', models.ForeignKey(related_name=b'+', blank=True, to='fifa.Csapat', null=True)),
                ('harmadik', models.ForeignKey(related_name=b'+', blank=True, to='fifa.Csapat', null=True)),
                ('masodik', models.ForeignKey(related_name=b'+', blank=True, to='fifa.Csapat', null=True)),
                ('negyedik', models.ForeignKey(related_name=b'+', blank=True, to='fifa.Csapat', null=True)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='merkozes',
            unique_together=set([('liga', 'home', 'away')]),
        ),
        migrations.AddField(
            model_name='golok',
            name='golszerzo',
            field=models.ForeignKey(to='fifa.Golszerzo'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='golok',
            name='merkozes',
            field=models.ForeignKey(to='fifa.Merkozes'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='csapat',
            name='ember',
            field=models.ForeignKey(to='fifa.Ember'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='bajnoksag',
            name='osztalyozo_A7_B4',
            field=models.ForeignKey(related_name=b'+', blank=True, to='fifa.Merkozes', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='bajnoksag',
            name='osztalyozo_A8_B3',
            field=models.ForeignKey(related_name=b'+', blank=True, to='fifa.Merkozes', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='bajnoksag',
            name='osztalyozo_B7_C4',
            field=models.ForeignKey(related_name=b'+', blank=True, to='fifa.Merkozes', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='bajnoksag',
            name='osztalyozo_B8_C3',
            field=models.ForeignKey(related_name=b'+', blank=True, to='fifa.Merkozes', null=True),
            preserve_default=True,
        ),
    ]
