from django import template
from django.core.urlresolvers import reverse

register = template.Library()

@register.simple_tag
def get_active_link(request,url,string):
    kwargs = {
        'full_url': reverse(url),
        'string': string,
        'class': ""
    }
    if url in request.get_full_path():
        kwargs['class'] = ' class="active"'

    return u'<li{class}><a href="{full_url}">{string}</a></li>'.format(**kwargs)
