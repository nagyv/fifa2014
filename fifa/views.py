from django.views.generic import ListView, DetailView, TemplateView
from .models import Ember, Csapat, Bajnoksag, Liga

class HomeView(TemplateView):
    template_name = 'fifa/home.html'

class EmberList(ListView):
    model = Ember

class CsapatList(ListView):
    model = Csapat

class BajnoksagList(ListView):
    model = Bajnoksag

# class BajnoksagView(DetailView):
#     model = Bajnoksag

class LigaView(DetailView):
    model = Liga