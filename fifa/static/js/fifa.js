if (window.location.hash) {
    $(window.location.hash).collapse('show');
    $('html, body').animate({
        scrollTop: $(window.location.hash).offset().top-100
    }, 2000);
}
$('table.table-sortable').tablesorter();