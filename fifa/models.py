from django.db import models
from model_utils import Choices
from model_utils.models import TimeStampedModel

pontozas = [50, 45, 40, 35, 32, 30, 29, 28, 27, 26,
            25, 20, 18, 17, 16, 15, 14, 13, 12, 11,
            10, 5, 4, 2, 1, 0, 0, 0, 0, 0]

class Ember(TimeStampedModel):
    name = models.CharField(max_length=255)
    pontszam = models.IntegerField(default=0)

    class Meta:
        ordering = ['-pontszam', 'name']

    def __unicode__(self):
        return self.name

    def pontok_szamitasa(self):
        self.pontszam = self.csapat_set.all().aggregate(sum_pontszam=models.Sum('pontszam'))['sum_pontszam']
        self.save()


class Csapat(models.Model):
    ember = models.ForeignKey(Ember)

    name = models.CharField(max_length=255)
    fifa_rangsor = models.SmallIntegerField(blank=True, null=True)
    ea_pontok = models.SmallIntegerField(blank=True, null=True)
    pontszam = models.IntegerField(default=0)

    class Meta:
        ordering = ['-pontszam', 'ea_pontok']

    def __unicode__(self):
        return u"%s (%s)" % (self.name, self.ember)

    def participations(self):
        ligak = list(Liga.objects.by_csapat(self)) + list(Selejtezo.objects.by_csapat(self))
        return ligak


class Bajnoksag(TimeStampedModel):
    idopont = models.DateField()

    osztalyozo_A7_B4 = models.ForeignKey('Merkozes', blank=True, null=True, related_name="+")
    osztalyozo_A8_B3 = models.ForeignKey('Merkozes', blank=True, null=True, related_name="+")
    osztalyozo_B7_C4 = models.ForeignKey('Merkozes', blank=True, null=True, related_name="+")
    osztalyozo_B8_C3 = models.ForeignKey('Merkozes', blank=True, null=True, related_name="+")

    class Meta:
        ordering = ['idopont']

    def __unicode__(self):
        return u"%s" % self.idopont

    def pontok_szamitasa(self):
        for liga in self.liga_set.all():
            liga.pontok_szamitasa()

    def ligak_generalasa(self):
        """
        Az elozo liga eredmenyei alapjan letrehozza a kovetkezo liga beosztasat
        :return: Bajnoksag
        """
        if self.liga_set.count() > 0:
            raise ValueError('Mar vannak ligak')

        # find the previous
        previous = Bajnoksag.objects.filter(idopont__lt=self.idopont)[0]
        ligaA, ligaB, ligaC = previous.liga_set.all()[:]
        feljutok = previous.selejtezo_set.all()[0]

        def _create(liga, **csapatok):
            for idx in range(1, 11):
                csapatok.setdefault('cs%d' % idx, getattr(liga, 'cs%d' % idx))
            self.liga_set.create(tipus=liga.tipus, **csapatok)

        _create(ligaA,
                     cs7=previous.osztalyozo_A7_B4.winner,
                     cs8=previous.osztalyozo_A8_B3.winner,
                     cs9=ligaB.pos1, cs10=ligaB.pos2)
        _create(ligaB,
                cs1 = ligaA.pos9,
                cs2 = ligaA.pos10,
                cs3 = previous.osztalyozo_A8_B3.loser,
                cs4 = previous.osztalyozo_A7_B4.loser,
                cs7 = previous.osztalyozo_B7_C4.winner,
                cs8 = previous.osztalyozo_B8_C3.winner,
                cs9 = ligaC.pos1, cs10 = ligaC.pos2
                )
        _create(ligaC,
                cs1 = ligaB.pos9,
                cs2 = ligaB.pos10,
                cs3 = previous.osztalyozo_B8_C3.loser,
                cs4 = previous.osztalyozo_B7_C4.loser,
                cs7 = feljutok.elso,
                cs8 = feljutok.masodik,
                cs9 = feljutok.harmadik,
                cs10 = feljutok.negyedik
                )


class LigaManager(models.Manager):

    def by_csapat(self, csapat):
        filters = models.Q(cs1=csapat) | models.Q(cs2=csapat) | models.Q(cs3=csapat) | \
            models.Q(cs4=csapat) | models.Q(cs5=csapat) | models.Q(cs6=csapat) | \
            models.Q(cs7=csapat) | models.Q(cs8=csapat) | models.Q(cs9=csapat) | \
            models.Q(cs10=csapat)
        return self.get_queryset().filter(filters)


class Liga(TimeStampedModel):
    LIGAK = Choices("A", "B", "C")

    bajnoksag = models.ForeignKey(Bajnoksag)
    tipus = models.CharField(choices=LIGAK, max_length=1)

    cs1 = models.ForeignKey(Csapat, related_name="+")
    cs2 = models.ForeignKey(Csapat, related_name="+")
    cs3 = models.ForeignKey(Csapat, related_name="+")
    cs4 = models.ForeignKey(Csapat, related_name="+")
    cs5 = models.ForeignKey(Csapat, related_name="+")
    cs6 = models.ForeignKey(Csapat, related_name="+")
    cs7 = models.ForeignKey(Csapat, related_name="+")
    cs8 = models.ForeignKey(Csapat, related_name="+")
    cs9 = models.ForeignKey(Csapat, related_name="+")
    cs10 = models.ForeignKey(Csapat, related_name="+")

    pos1 = models.ForeignKey(Csapat, related_name="+", blank=True, null=True)
    pos2 = models.ForeignKey(Csapat, related_name="+", blank=True, null=True)
    pos3 = models.ForeignKey(Csapat, related_name="+", blank=True, null=True)
    pos4 = models.ForeignKey(Csapat, related_name="+", blank=True, null=True)
    pos5 = models.ForeignKey(Csapat, related_name="+", blank=True, null=True)
    pos6 = models.ForeignKey(Csapat, related_name="+", blank=True, null=True)
    pos7 = models.ForeignKey(Csapat, related_name="+", blank=True, null=True)
    pos8 = models.ForeignKey(Csapat, related_name="+", blank=True, null=True)
    pos9 = models.ForeignKey(Csapat, related_name="+", blank=True, null=True)
    pos10 = models.ForeignKey(Csapat, related_name="+", blank=True, null=True)

    objects = LigaManager()

    class Meta:
        ordering = ['bajnoksag', "tipus"]

    def __unicode__(self):
        return u"Liga %s (%s)" % (self.tipus, self.bajnoksag)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        # TODO: validation: find duplicates csapat
        return super(Liga, self).save(force_insert, force_update, using, update_fields)

    @models.permalink
    def get_absolute_url(self):
        return ('liga', [], {'pk': self.pk})

    def csapatok(self, by_position=False):
        """
        Returns all the teams.

        If the league by_position==True the teams are ordered by position.

        :param by_position:
        :return: iterator of the teams
        """
        for i in range(1,11):
            if by_position:
                yield getattr(self, 'pos%d' % i)
            else:
                yield getattr(self, 'cs%d' % i)

    def eredmenyek(self):
        return self.csapatok(True)

    def merkozesek_generalasa(self):
        """
        Miutan minden csapat adott, legeneralja az osszes merkozes part a ligaban

        :return:
        """
        if self.merkozes_set.count() > 0:
            raise ValueError('Mar vannak merkozesek')

        for idx in range(1, 11):
            home = getattr(self, 'cs%d' % idx)
            for away_idx in range(idx+1, 11):
                away = getattr(self, 'cs%d' % away_idx)
                self.merkozes_set.create(home=home, away=away, bajnoksag=self.bajnoksag)

    def pontok_szamitasa(self):
        for idx in range(1, 11):
            csapat = getattr(self, 'pos%d' % idx)
            if self.tipus == 'A':
                csapat.pontszam += pontozas[idx-1]
            elif self.tipus == 'B':
                csapat.pontszam += pontozas[9 + idx]
            else:
                csapat.pontszam += pontozas[19 + idx]
            csapat.save()

class SelejtezoManager(models.Manager):

    def by_csapat(self, csapat):
        filters = models.Q(cs1=csapat) | models.Q(cs2=csapat) | models.Q(cs3=csapat) | \
            models.Q(cs4=csapat) | models.Q(cs5=csapat) | models.Q(cs6=csapat) | \
            models.Q(cs7=csapat) | models.Q(cs8=csapat) | models.Q(cs9=csapat) | \
            models.Q(cs10=csapat) | models.Q(cs11=csapat) | models.Q(cs12=csapat) | \
            models.Q(cs13=csapat) | models.Q(cs14=csapat) | models.Q(cs15=csapat) | \
            models.Q(cs16=csapat)
        return self.get_queryset().filter(filters)

class Selejtezo(TimeStampedModel):
    bajnoksag = models.ForeignKey(Bajnoksag)

    cs1 = models.ForeignKey(Csapat, related_name="+")
    cs2 = models.ForeignKey(Csapat, related_name="+")
    cs3 = models.ForeignKey(Csapat, related_name="+")
    cs4 = models.ForeignKey(Csapat, related_name="+")
    cs5 = models.ForeignKey(Csapat, related_name="+")
    cs6 = models.ForeignKey(Csapat, related_name="+")
    cs7 = models.ForeignKey(Csapat, related_name="+")
    cs8 = models.ForeignKey(Csapat, related_name="+")
    cs9 = models.ForeignKey(Csapat, related_name="+")
    cs10 = models.ForeignKey(Csapat, related_name="+")
    cs11 = models.ForeignKey(Csapat, related_name="+")
    cs12 = models.ForeignKey(Csapat, related_name="+")
    cs13 = models.ForeignKey(Csapat, related_name="+")
    cs14 = models.ForeignKey(Csapat, related_name="+")
    cs15 = models.ForeignKey(Csapat, related_name="+")
    cs16 = models.ForeignKey(Csapat, related_name="+")

    elso = models.ForeignKey(Csapat, related_name="+", blank=True, null=True)
    masodik = models.ForeignKey(Csapat, related_name="+", blank=True, null=True)
    harmadik = models.ForeignKey(Csapat, related_name="+", blank=True, null=True)
    negyedik = models.ForeignKey(Csapat, related_name="+", blank=True, null=True)

    objects = SelejtezoManager()

    def __unicode__(self):
        return u"Selejtezo %s" % self.bajnoksag

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        # TODO: validation: find duplicates csapat
        return super(Selejtezo, self).save(force_insert, force_update, using, update_fields)

class GolszerzoManager(models.Manager):

    def with_all_goals(self):
        return self.get_queryset().filter(golok__ongol=False).annotate(total_goals=models.Sum('golok__golok'))


class Golszerzo(TimeStampedModel):
    csapat = models.ForeignKey(Csapat)
    name = models.CharField(max_length=255)
    osszgol = models.IntegerField(default=0)

    objects = GolszerzoManager()

    class Meta:
        ordering = ['osszgol']

    def __unicode__(self):
        return u"%s (%s)" % (self.name, self.csapat)


class Merkozes(TimeStampedModel):
    bajnoksag = models.ForeignKey(Bajnoksag)
    liga = models.ForeignKey(Liga, null=True, blank=True)  # null==True, ha osztalyozo

    home = models.ForeignKey(Csapat, related_name="merkozes_home")
    away = models.ForeignKey(Csapat, related_name="merkozes_away")
    home_score = models.SmallIntegerField(blank=True, null=True)
    away_score = models.SmallIntegerField(blank=True, null=True)
    gol = models.ManyToManyField(Golszerzo, through='Golok')

    @property
    def winner(self):
        return self.home if self.home_score > self.away_score else self.away

    @property
    def loser(self):
        return self.home if self.home_score < self.away_score else self.away

    class Meta:
        unique_together = (('liga', 'home', 'away'),)

    def __unicode__(self):
        return u"%s - %s (%s)" % (self.home, self.away, self.bajnoksag)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.liga and self.liga.bajnoksag != self.bajnoksag:
            raise ValueError('A liga nem az adott bajnoksaghoz tartozik.')
        return super(Merkozes, self).save(force_insert, force_update, using,
             update_fields)


class Golok(TimeStampedModel):
    golszerzo = models.ForeignKey(Golszerzo)
    merkozes = models.ForeignKey(Merkozes)
    golok = models.SmallIntegerField(default=0)
    ongol = models.BooleanField(default=False, blank=True)
