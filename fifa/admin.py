# -*- coding: utf-8 -*-
from django.contrib import admin
from models import Ember, Csapat, Bajnoksag, Liga, Selejtezo, Merkozes, Golok, Golszerzo
import re

csapat1Regex = re.compile('^cs(\d+)$')
csapat2Regex = re.compile('^p(\d+)$')

class GolokInline(admin.TabularInline):
    model = Golok
    extra = 1
    # fields = ['golszerzo']
    # raw_id_fields = ("golszerzo",)

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        # if db_field.name == 'golszerzo':
        #     import ipdb; ipdb.set_trace()
        #     merkozes_id = int(request.resolver_match.args[0])
        #     merkozes = Merkozes.objects.get(pk=merkozes_id)
        #     kwargs['queryset'] = Golszerzo.objects.filter(csapat_id__in=[merkozes.away_id, merkozes.home_id])
        super(GolokInline, self).formfield_for_foreignkey(db_field, request=request, **kwargs)


class MerkozesInline(admin.TabularInline):
    model = Merkozes
    extra = 0
    can_delete = False
    fields = ('home_score', 'away_score')

    # def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
    #     if db_field.name in ['home', 'away']:
    #         kwargs['queryset'] = None
    #     super(MerkozesInline, self).formfield_for_foreignkey(db_field, request=request, **kwargs)

@admin.register(Ember)
class EmberAdmin(admin.ModelAdmin):
    actions = ['pontok_szamitasa']

    def pontok_szamitasa(self, request, queryset):
        for ember in queryset:
            ember.pontok_szamitasa()
    pontok_szamitasa.short_description = 'Pontok számítása'

@admin.register(Csapat)
class CsapatAdmin(admin.ModelAdmin):
    # filter_horizontal = ['ember']
    list_display = ('name', 'ember', 'pontszam', 'ea_pontok', 'fifa_rangsor')

@admin.register(Bajnoksag)
class BajnoksagAdmin(admin.ModelAdmin):
    date_hierarchy = 'idopont'
    actions = ['pontok_szamitasa']

    def pontok_szamitasa(self, request, queryset):
        for bajnoksag in queryset:
            bajnoksag.pontok_szamitasa()
    pontok_szamitasa.short_description = 'Pontok számítása'

@admin.register(Liga)
class LigaAdmin(admin.ModelAdmin):
    actions = ['merkozesek_generalasa']
    inlines = [MerkozesInline]
    list_display = ('tipus', 'bajnoksag')
    list_filter = ('tipus', 'bajnoksag__idopont')
    list_select_related = ('bajnoksag',)

    def merkozesek_generalasa(self, request, queryset):
        for liga in queryset:
            liga.merkozesek_generalasa()
    merkozesek_generalasa.short_description = "Mérkőzések generálása"

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        if not hasattr(self, 'csapat1List'):
            self.csapat1List = Csapat.objects.all()

        if csapat1Regex.findall(db_field.name):
            kwargs["queryset"] = self.csapat1List

        if csapat2Regex.findall(db_field.name):
            if not hasattr(self, 'csapat2List'):
                self.csapat2List = Csapat.objects.all()
            kwargs["queryset"] = self.csapat2List

        return super(LigaAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)


@admin.register(Merkozes)
class MerkozesAdmin(admin.ModelAdmin):
    inlines = [GolokInline]

@admin.register(Golszerzo)
class GolszerzoAdmin(admin.ModelAdmin):
    inlines = [GolokInline]
    search_fields = ('name',)

admin.site.register(Selejtezo)
